#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Points to the TIGER-corpus-of-German download page on the internets.

import webbrowser

TIGER_LICENSE = 'http://www.ims.uni-stuttgart.de/forschung/ressourcen/' +\
                'korpora/TIGERCorpus/license/htmlicense.html'

print('You need to accept the license terms to download the TIGER corpus.\n')

webbrowser.open(TIGER_LICENSE)

print('Please place the downloaded .conll09 file into the '
      'acceptance-tests/T_RoundTripTranslation folder.')

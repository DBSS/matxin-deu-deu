#!/usr/bin/env bash

./helpers/apache-opennlp-1.6.0/bin/opennlp TokenizerME helpers/models/de-token.bin 2>/dev/null | \
    java -cp helpers/transition-1.30.jar is2.util.Split /dev/stdin | \
    java -Xmx2G -classpath helpers/transition-1.30.jar is2.lemmatizer2.Lemmatizer &>/dev/null \
        -model helpers/models/ger-tagger+lemmatizer+morphology+graph-based-3.6/lemma-ger-3.6.model -uc \
        -test /dev/stdin -out /tmp/lemmatized

java -Xmx3g -classpath helpers/transition-1.30.jar is2.transitionS2a.Parser &>/dev/null -model helpers/models/pet-ger-S2a-40-0.25-0.1-2-2-ht4-hm4-kk0 -test /tmp/lemmatized -out /tmp/tagged-parsed

cat /tmp/tagged-parsed | awk -F"\t" 'BEGIN{OFS="\t";} {if (NF==0) print; else print "1_"$1, $2, $4, "_", $6, "_",  $8, "_", $10, "_", $12, "_", "_", "_", "_";}'

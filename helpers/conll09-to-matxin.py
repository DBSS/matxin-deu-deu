#!/usr/bin/env python3

## A CoNLL-2009 to Matxin format converter.
##
## Adapted from: https://github.com/ftyers/ud-scripts/blob/master/conllu-to-matxin.py
##
## INPUT:
##
## 1_1     Ein     ein     _       ART     _       nom|sg|masc     _       2       _       NK      _       _       _       _
## 1_2     Sprecher        Sprecher        _       NN      _       nom|sg|masc     _       5       _       SB      _       _       _       _
## 1_3     des     der     _       ART     _       gen|sg|neut     _       4       _       NK      _       _       _       _
## 1_4     Außenministerium        Außenministerium        _       NN      _       gen|sg|neut     _       2       _       AG      _       _       _       _
## 1_5     spricht sprechen        _       VVFIN   _       sg|3|pres|ind   _       0       _       --      _       _       _       _
## 1_6     .       --      _       $.      _       _       _       5       _       --      _       _       _       _
##
## OUTPUT:
##
## <corpus>
##   <SENTENCE ord="1" alloc="0">
##     <NODE ord="5" alloc="0" form="spricht" lem="sprechen" mi="VVFIN|sg|3|pres|ind" si="--">
##       <NODE ord="2" alloc="0" form="Sprecher" lem="Sprecher" mi="NN|nom|sg|masc" si="SB">
##         <NODE ord="1" alloc="2" form="Ein" lem="ein" mi="ART|nom|sg|masc" si="NK"/>
##         <NODE ord="4" alloc="2" form="Außenministerium" lem="Außenministerium" mi="NN|gen|sg|neut" si="AG">
##           <NODE ord="3" alloc="4" form="des" lem="der" mi="ART|gen|sg|nt" si="NK"/>
##         </NODE>
##       </NODE>
##       <NODE ord="6" alloc="5" form="." lem="--" mi="$." si="--"/>
##     </NODE>
##   </SENTENCE>
## </corpus>

import sys ;

capcalera = """<?xml version='1.0' encoding='UTF-8' ?>
<corpus>
""" ; 

print(capcalera);
scount = 0;
lcount = 0;
ccount = 0;

deps = {}; 
nodes = {};

def escape(s): #{
	o = s;
	o = o.replace('"', '&#34;');
	o = o.replace("'", '&quot;');
	o = o.replace("&", '&amp;');
	return o;
#}

def proc(depth, nodes, deps, node): #{
	depth = depth + 1;
	if node != 0: #{
		form = escape(nodes[node][1]);
		lem = escape(nodes[node][2]);
		alloc = int(escape(nodes[node][8]));
		mi = '_';
		if nodes[node][4] != '_': #{
			mi = nodes[node][4] + '|' + nodes[node][6];
		elif nodes[node][3] != '_': #{
			mi = nodes[node][3] + '|' + nodes[node][5];
		#}
		mi = mi.replace('|_', '').replace('<', '[').replace('>', ']');
		si = nodes[node][10].replace('>', '→').replace('<', '←');
		if node in deps and len(deps[node]) > 0: #{
			print(' ' * (2 * depth), '<NODE ord="%d" alloc="%d" form="%s" lem="%s" mi="%s" si="%s">' % (node, alloc, form, lem, mi, si) );
		else: #{
			print(' ' * (2 * depth), '<NODE ord="%d" alloc="%d" form="%s" lem="%s" mi="%s" si="%s"/>' % (node, alloc, form, lem, mi, si) );
		#}
	#}
	if node in deps: #{
		for n in deps[node]: #{
			proc(depth, nodes, deps, n);
		#}
	else: #{
		return;
	#}
	if node != 0: #{
		print(' ' * (2 * depth), '</NODE>');
	#}
	depth = depth - 1;
	return ;
#}
open = 0;
ord = 0;
for line in sys.stdin.readlines(): #{
	line = line.strip('\n');
	if line.count('# ord:') > 0: #{
		line = line.replace('\t_', '\t');
		ord = int(line.split('ord:')[1].strip().split(' ')[0].strip());
	elif line == '\n': #{
		ord = 0;
	#}
	if line.count('\t') > 1 and line[0] != '#': #{
		row = line.split('\t');
		row[0] = row[0].split('_')[1]
		if row[0] == '1': #{
			scount = scount + 1;
			print('<SENTENCE ord="%d" alloc="%d">' % (ord, ccount)) ;
			open = 1;
		#}
		if row[0].count('-') > 0: #{
			continue;
		#}
		cur = int(row[0]);
		cap = int(row[8]);
		if cap not in deps: #{
			deps[cap] = [];
		#}
		deps[cap].append(cur);
		nodes[cur] = row; 
	#}
	if line.strip() == '' and open == 1: #{
		proc(0, nodes, deps, 0); 
		print('</SENTENCE>');
		open = 0;
		deps = {};
		nodes = {};
	#}
#}

print('\n</corpus>');


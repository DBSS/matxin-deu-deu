#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Given a sentence in matxin format, linearize it according to the 'ref'
## value of nodes and output a string.

import xml.etree.ElementTree as ET
import sys
from operator import itemgetter

## ElementTree -> String
def proc_nodes(sent):
    """Return a linearized string given SENTENCE node."""
    nodes = []
    for e in sent.iter():
        if e.tag == 'NODE':
            nodes.append((int(e.attrib['ref']), e.attrib['form']))
    return ' '.join([form for ref, form in sorted(nodes, key=itemgetter(0))])


tree = ET.parse(sys.stdin)

for e in tree.iter():
    if e.tag == 'SENTENCE':
        print(proc_nodes(e) + '\n')

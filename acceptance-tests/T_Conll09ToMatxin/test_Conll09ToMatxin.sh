#!/usr/bin/env bash

# Feature: Convert Conll-2009 files into format expected by Matxin version 2.

if diff <(cat acceptance-tests/T_Conll09ToMatxin/input.txt | \
          ./helpers/conll09-to-matxin.py) \
        acceptance-tests/T_Conll09ToMatxin/expected.txt

then
  echo "PASSED: $0"
  exit 0

else
  echo "FAILED: $0"
  exit 1

fi

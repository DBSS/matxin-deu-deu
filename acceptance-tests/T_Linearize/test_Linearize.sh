#!/usr/bin/env bash

# Feature: Form a sentence by ordering forms according to the 'ref'
#          value of the nodes they appear in.

if diff <(cat acceptance-tests/T_Linearize/input.txt | ./helpers/linearize.py) \
        acceptance-tests/T_Linearize/expected.txt

then
  echo "PASSED: $0"
  exit 0

else
  echo "FAILED: $0"
  exit 1

fi

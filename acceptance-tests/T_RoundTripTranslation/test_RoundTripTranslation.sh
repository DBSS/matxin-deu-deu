#!/usr/bin/env bash

# Feature: Translating the sentences from the TIGER corpus and then back
#          does not change sentences.

all_scenarios_passed=true


# Scenario 1: When no transfer module is involved, passing a corpus through
#             matxin does not change it.
    if diff <(cat acceptance-tests/T_RoundTripTranslation/tiger_release_aug07.corrected.16012013.conll09 | \
              ./helpers/conll09-to-matxin.py | \
              matxin-xfer-lex deu-deu.autobil.bin | \
              matxin-generate deu.gnx.bin deu.autogen.bin | \
              ./helpers/matxin-to-conll09.py) \
            acceptance-tests/T_Linearize/expected.txt
    then
        echo "     PASSED: Scenario 2 of $0"
        exit 0
    else
        all_scenarios_passed=false
        echo "     FAILED: Scenario 2 of $0"
        exit 1
    fi


# Scenario 2: The gold standard dependency parses from the TIGER corpus
# do not change when run through matxin.
    if diff <(cat acceptance-tests/T_RoundTripTranslation/tiger_release_aug07.corrected.16012013.conll09 | \
              ./helpers/conll09-to-matxin.py | \
              matxin -d . deu-deu | matxin -d . deu-deu | \
              ./helpers/matxin-to-conll09.py) \
            acceptance-tests/T_Linearize/expected.txt
    then
        echo "     PASSED: Scenario 2 of $0"
        exit 0
    else
        all_scenarios_passed=false
        echo "     FAILED: Scenario 2 of $0"
        exit 1
    fi
    

# Scenario 3: The dependency parses which the MATE tools parser gives for the
#             sentences in the TIGER corpus do not change when run through
#             matxin.
    if diff <(cat acceptance-tests/T_RoundTripTranslation/tiger_release_aug07.corrected.16012013.conll09 | \
              ./helpers/conll09-to-matxin.py | \
              matxin -d . deu-deu | matxin -d . deu-deu | \
              ./helpers/matxin-to-conll09.py) \
            acceptance-tests/T_Linearize/expected.txt
    then
        echo "     PASSED: Scenario 3 of $0"
        exit 0
    else
        all_scenarios_passed=false
        echo "     FAILED: Scenario 3 of $0"
        exit 1
    fi


if [ "$all_scenarios_passed" = true ]; then
    echo "PASSED: $0"
    exit 0;
else
    echo "FAILED: $0"
    exit 1
fi
